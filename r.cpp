#include <iostream> 
#include <vector>
using namespace std;

vector< vector<int> > pathlist;
vector<int> recurse(int seed, int k, vector<int> path);

int main(){
  vector<int> path;
  int k = 3;
  for(int i = 0; i < 4; i++){
    recurse(i,k,path);// = recurse(i,k,path);
  }
  for(int i = 0; i < pathlist.size(); i++){
    for(int j = 0; j < (pathlist[i]).size(); j++){
      cout << pathlist[i][j] << " ";
    }
    cout << endl;
  }
  return 0;
}

vector<int> recurse(int seed, int k, vector<int> path){
  //cout << "Entry length: " << path.size() << endl;
  vector<int> newPath = path;
  //cout << "Entrynew length: " << newPath.size() << endl;
  if(k == 0){
    //for(int i = 0; i < newPath.size(); i++){
    //  cout << newPath[i] << " ";
    //}
    //cout << endl;
    pathlist.push_back(newPath);
    return newPath;
  }
    //cout << "pushing " << (i+1)*k << endl;
    newPath.push_back((seed+1)*k);
    //cout << "before recursion length: " << newPath.size() << endl;
    if(k == 2){
      recurse(seed+4, k-1, newPath);
      recurse(seed+6, k-1, newPath);
    }
    else recurse(seed, k-1, newPath);
}
