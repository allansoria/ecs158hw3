// g++ -std=c++11 -Ofast dfs.cpp -fopenmp && time ./dfs
#include <iostream>
#include <utility>
#include <stdlib.h>
#include <vector>
#include <omp.h>
using namespace std;

vector< vector<int> > pathsList;
vector< vector<int> > edgesList;

int** pathsListA;

vector< vector<int> > listE(int *adjm, int n);
void dfs(int depth, int v, vector<int> path, int* pathpointer);
void printVector(vector<int> a);
void findpaths(int *adjm, int n, int k, int *paths, int *numpaths);



int main(){
  int n = 30; // Number of vertices in graph
  //20 = 49521980    40s
  //                 28s basic omp for + crit
  // 5 = 5120
  int k = 5; // Length of each path
  int a[] = {0 ,1 ,0 ,1 ,0,
             0 ,0 ,1 ,1 ,0, 
             0 ,0 ,0 ,1 ,0, 
             0 ,0 ,0 ,0 ,1, 
             0 ,0 ,1 ,0 ,0 };
  int b[] = {0 ,1 ,0 ,1 ,0,
             0 ,0 ,1 ,1 ,0,
             0 ,1 ,0 ,1 ,0,
             0 ,1 ,0 ,0 ,1,
             0 ,1 ,1 ,0 ,0 };
  int c[] = {0 ,1 ,1 ,1 ,1,
             1 ,0 ,1 ,1 ,1,
             1 ,1 ,0 ,1 ,1,
             1 ,1 ,1 ,0 ,1,
             1 ,1 ,1 ,1 ,0 };
  int *d;
  d = (int*)malloc(sizeof(int)*n*n);
  //10000
  for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
      if(i == j){
        d[n*i+j] = 1;
      }
      else{
        d[n*i+j] = 1;
      }
    }
  }

  for(int i = 0; i < n*n; i++){
    if(i % n == 0){
      cout << endl;
    } 
    cout << d[i];
  }
  cout << endl;
  int *paths;
  int *numpaths;
  //findpaths(a,n,k, paths, numpaths);
  //findpaths(b,n,k, paths, numpaths);
  //findpaths(c,n,k, paths, numpaths);
  findpaths(d,n,k, paths, numpaths);
  //get list of edges for all vertices
  //[v][e]
  /*edgesList = listE(a,n);
  for(int i = 0; i < n; i++){
    //cout << "Running dfs with seed: " << i << endl;
    vector<int> path;
    //initialize path with first value
    path.push_back(i);
    dfs(4, i, path);
  }*/
  // PRINT ALL PATHS
  /*for(int i = 0; i < pathsList.size(); i++){
    cout << "Path: ";
    for(int j = 0; j < pathsList[i].size(); j++){
      cout << pathsList[i][j] << " ";
    }
    cout << endl;
  }*/
}

void findpaths(int *adjm, int n, int k, int *paths, int *numpaths){
  pathsList.clear();
  edgesList.clear();
  edgesList = listE(adjm, n);
  int numberOfPaths = 0;
  #pragma omp parallel for
    for(int i = 0; i < n; i++){
      vector<int> path;
      int* pathpointer;
      pathpointer = (int*)malloc(k*sizeof(int));
      pathpointer[0] = i;
      path.push_back(i);
      dfs(k, i, path, pathpointer);
    }
    numberOfPaths = pathsList.size();
    numpaths = &numberOfPaths;
    cout << *numpaths << endl;
}

void dfs(int depth, int v, vector<int> path, int* pathpointer, int*){
  if(depth ==0){
      #pragma omp critical
      {
        pathsList.push_back(move(path));
      }
    return;
  }
  else{
    for(int i = 0; i < edgesList[v].size(); i++){
      //int edge = edgesList[v][i];
      vector<int> newPath = path;
      newPath.push_back(move(edgesList[v][i]));
      dfs(depth-1, edgesList[v][i], newPath);
      //path.push_back(edge);
      //dfs(depth-1, edge, path);
    }
  }
}

vector< vector<int> > listE(int *adjm, int n ){
  vector< vector<int> > list; //makes vector of size n
  for(int i = 0; i < n; i++ ){
    vector<int> edges;
    for (int j= 0; j < n; j++){
      if(adjm[n*i+j] == 1)
        edges.push_back(j);
    }
    list.push_back(edges);
  }
  return list;
}

void printVector(vector<int> a){
  for(int i = 0; i < a.size(); i++){
    cout << a[i] << " ";
  }
  cout << endl;
}

