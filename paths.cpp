#include <iostream>
#include <stdlib.h>
#include <iostream>
#include <vector>
using namespace std;

#define STARTVERTEX 0
vector< vector<int> > listPaths;
/*
adjm:     Adjacency matrix of a directed graph
n:        Number of vertices in the graph
k:        Length of each path
paths:    Matrix of paths
numpaths: Number of paths

The output matrix, paths, will have 'numpaths' filled rows with 'k+1' columns, 
and likely unused rows at the bottom. The space will be preallocated by the caller.

*/

vector< vector<int> > edgeList;
//outNodes will be the outgoing edges from a node n
//ex:
// 1: 2 4
// 2: 3 4
// 3: 4
// 4: 5
// 5: 3

void findpaths(int *adjm, int n, int k, int *paths, int *numpaths);
vector< vector<int> > listE(int *adjm, int n);
void initializeDFS(vector< vector<int> > &listEdges, vector< vector<int> > &listPaths);
//void dfs(vector< vector<int> > &listPaths, vector< vector<int> > &listEdges, int targetDepth, int currentNode);
void printVectorVectorInt(vector< vector<int> > list);
void printVector(vector<int> a);
void dfs(vector< vector<int> > &listEdges, int depth, int v, vector<int> path);

int main(){
  int n = 5; // Number of vertices in graph
  int k = 5; // Length of each path
  int a[] = {0 ,1 ,0 ,1 ,0
    ,0 ,0 ,1 ,1 ,0,
      0, 0 ,0 ,1 ,0,
      0 ,0, 0, 0 ,1,
      0 ,0 ,1, 0 ,0 };
  //get list of edges for all vertices
  //[v][e]
  vector< vector<int> > listEdges = listE(a,n);
  initializeDFS(listEdges, listPaths);
  //cout << endl;
  printVectorVectorInt(listEdges);
  cout << endl;
  vector<int> test;
  printVectorVectorInt(listPaths);
  for(int i = 0; i < n; i++){
    vector<int> path;
    cout << "Running dfs with seed: " << i << endl;
    dfs(listEdges, k, i, path);
  }
  //dfs(listPaths, listEdges, k, STARTVERTEX); 
}

void dfs(vector< vector<int> > &listEdges, int depth, int v, vector<int> path){
  //printVector(path);
  if(depth == 0){
    /*for(int i = 0; i < path.size(); i++){
      cout << path[i] << " ";
    }
      cout << endl;*/
      listPaths.push_back(path);
      return;
  }
  for(int i = 0; i < listEdges[v].size(); i++){
    vector<int> newPath = path;
    newPath.push_back(listEdges[v][i]);
    printVector(newPath);
    dfs(listEdges, depth - 1, listEdges[v][i], newPath);
  }
  printVectorVectorInt(listPaths);
}

void printVector(vector<int> a){
  for(int i = 0; i < a.size(); i++){
    cout << a[i] << " ";
  }
  cout << endl;
}

//void dfs(vector< vector<int> > &listPaths, vector< vector<int> > &listEdges, int depth, int currentNode){
//  
//}
/*
void dfs(vector< vector<int> > &listPaths, vector< vector<int> > &listEdges, int depth,  int currentNode){
  int startingListPathSize = listPaths.size();
  for(int i = 0; i < startingListPathSize; i++){
    int pathsFromCurrentTail = listEdges[listPaths[i].back()].size();
    //cout << listPaths[i].back() << " has " << pathsFromCurrentTail << " edges" << endl;
    if(pathsFromCurrentTail>1){
      //cout << "Need to add " << pathsFromCurrentTail - 1 << " lists" << endl;
      //cout << "Current path: ";
      for(int a = 0; a < pathsFromCurrentTail - 1; a++){
      vector<int> newPath;
      for(int k = 0; k < listPaths[i].size();k++){
        newPath.push_back(listPaths[i][k]);
        //cout << listPaths[i][k] << " ";
      }
      //cout << endl;
      listPaths.push_back(newPath);
      }
    }
  }
  printVectorVectorInt(listPaths);
}*/
/*
   initialize the listPaths with first set of rows
      0: 0 1
      1: 0 3
*/
void initializeDFS(vector< vector<int> > &listEdges, vector< vector<int> > &listPaths){
  for(int j = 0; j < listEdges.size(); j++){
  for(int i = 0; i < (listEdges)[j].size(); i++){
    vector<int> path;
    path.push_back(j);
    path.push_back((listEdges)[j][i]);
    listPaths.push_back(path);
  }
  }
}
//listE will return a list of lists comprised of all outgoing edges for each vertex
vector< vector<int> > listE(int *adjm, int n ){
  vector< vector<int> > list; //makes vector of size n

  for(int i = 0; i < n; i++ ){
    vector<int> edges;
    for (int j= 0; j < n; j++){
      if(adjm[n*i+j] == 1)
        edges.push_back(j);
    }
    list.push_back(edges);
  }
  return list;
}
void printVectorVectorInt(vector< vector<int> > list){
  for(int i = 0; i < list.size(); i++){
    cout <<  "Path: ";
    for(int j = 0; j < list[i].size(); j++){
      cout << list[i][j] << " ";
    }
    cout << endl;
  }
}
