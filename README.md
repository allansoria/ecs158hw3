# ecs158hw3
Homework III

Due Wed, May 11

Note: As always, you are welcome to search the Internet for ideas on how to write your code. You will rarely if ever find something that fully matches our assignments, but in any case you must cite any source that you rely on heavily.

The code here is easy to write in slow versions, but challenging to make it fast.

Problem 1

Here you will write OpenMP code to find all possible paths of a given length, starting from a given vertex. The signature of the function will be

void findpaths(int *adjm, int n, int k, int *paths, int *numpaths)
where the arguments are:

adjm: Adjacency matrix of a directed graph.
n: Number of vertices in the graph.
k: Length of each path.
paths: Matrix of paths.
numpaths: Number of paths.
The output matrix, paths, will have numpaths filled rows and k+1 columns, and likely unused rows at the bottom. The space will be pre-allocated by the caller.

For example, if k is 5 and row i of paths is

99 2 0 3 8 88
this is the path 99 → 2 → 0 → 3 → 8 → 88

Loops are allowed, e.g. 5 → 2 → 0 → 2 → 0 → 88

Place your code in a file paths.cpp

The groups with the fastest 3 versions will receive Extra Credit.

Problem 2:

Here you will write GPU code to do NMF.

Your function will have signature

void nmfgpu(float *a, int r, int c, int k, int niters, float *w, float *h)
with the same roles for the arguments as in Homework I.

Place your code in the file nmfgpu.cpp.

Problem 3:

Write an R function with "declaration"

nmfgpu <- function(a,k)
that finds and returns the rank-k NMF approximation to the matrix a. It will call a modified version of your code in Problem 2, using Rcpp.

Again, Extra Credit will go to the fastest 3 submissions.

Place your C/C++ code in the file nmfgpur.cpp, and your R code in nmf.R.
